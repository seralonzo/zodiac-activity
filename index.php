<!DOCTYPE html>
<html>
<head>
	<title>Zodiac</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.css">
</head>
	<body class="bg-dark">
        <h1 class="text-center text-uppercase p-5"></h1>
        <div class="col-lg-4 offset-lg-4">
                <form action="controller/zodiac-controller.php" method="POST" class="bg-light p-4">
                        <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                                <label for="birthmonth">Birth Month</label>
                                <input type="number" name="birthmonth" class="form-control">
                        </div>
                         <div class="form-group">
                                <label for="birthday">Birth Day</label>
                                <input type="number" name="birthday" class="form-control">
                        </div>
                        <div class="text-center">
                                <button type="submit" class="btn btn-primary">Check Horoscope</button>
                        </div>
                </form>
        </div>

</body>
</html>